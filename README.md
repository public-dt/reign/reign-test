Daniel Toro Test Reign
===========================================

## Getting Started

Clone project.
```sh
$ git clone https://gitlab.com/public-dt/reign/reign-test.git
```

Go to folder.
```sh
$ cd reign-test
```

Clone Client and Server projects.

```sh
$ git clone https://gitlab.com/public-dt/reign/client.git && \
git clone https://gitlab.com/public-dt/reign/server.git
```
Build containers with
```sh
$ docker-compose build
```

Raise up all container and services with
```sh
$ docker-compose up
```

Go to browser and go to the next url
```
http://localhost
```

When the server starts, the function "getPost" is executed, which obtains the initial data from http://hn.algolia.com/api/v1/search_by_date?query=nodejs.

#### Project structure

```
.
├── README.md
├── client
├── docker-compose.yml
└── server
```

#### Server structure

```
.
├── Dockerfile
├── README.md
├── __test__
│   ├── index.spec.js
│   └── utils
│       └── db-handler.js
├── gitlab-ci.yml
├── package-lock.json
├── package.json
└── src
    ├── controllers
    │   ├── index.js
    │   └── postController.js
    ├── index.js
    ├── jobs
    │   └── index.js
    ├── models
    │   ├── Post.js
    │   └── index.js
    ├── routes
    │   ├── index.js
    │   └── post.js
    └── services
        ├── index.js
        └── postService.js
```

#### Client structure

```
.
├── Dockerfile
├── README.md
├── package-lock.json
├── package.json
├── public
│   ├── favicon.ico
│   ├── index.html
│   ├── logo192.png
│   ├── logo512.png
│   ├── manifest.json
│   └── robots.txt
├── src
│   ├── App.css
│   ├── App.js
│   ├── App.test.js
│   ├── components
│   │   ├── Header
│   │   │   └── index.js
│   │   └── Posts
│   │       └── index.js
│   ├── hooks
│   ├── index.css
│   ├── index.js
│   ├── logo.svg
│   ├── serviceWorker.js
│   └── setupTests.js
└── yarn.lock
```

---
Thanks for the opportunity!

Best regards!